const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');
const weatherRouter = require('./routers/weatherRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);
app.use('/api/weather', weatherRouter);
app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect(
        'mongodb+srv://mytaskapp:mytaskapp@cluster0.sd3er.mongodb.net/uber-like-api?retryWrites=true&w=majority',
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useCreateIndex: true,
        },
    );
  } catch (e) {
    res.status(500).json({message: e.message});
  }

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
