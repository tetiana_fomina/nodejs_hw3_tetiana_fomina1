const sgMail = require('@sendgrid/mail');
const {SENDGRID_API_KEY} = require('../config');

sgMail.setApiKey(SENDGRID_API_KEY);

const sendEmailWithNewPassword = (email) => {
  sgMail
      .send({
        to: email,
        from: 'tetiana_fomina1@epam.com',
        subject: 'Your new password is inside this mail!',
        text: `Hello, ${email}. Your new password is "qwerty123"`,
      })
      .catch((e) => new Error(e.message));
};

module.exports = {
  sendEmailWithNewPassword,
};
