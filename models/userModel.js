const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
      email: {
        type: String,
        required: true,
        uniqe: true,
      },
      password: {
        type: String,
        required: true,
      },
      role: {
        type: String,
        required: true,
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
      tokens: [
        {
          token: {
            type: String,
          },
        },
      ],
      avatar: {
        type: Buffer,
      },
    },
    {
      collection: 'users',
    },
);

userSchema.virtual('trucks', {
  ref: 'Truck',
  localField: '_id',
  foreignField: 'created_by',
});

userSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'created_by',
});
userSchema.methods.toJSON = function() {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.tokens;
  delete userObject.__v;
  delete userObject.role;
  delete userObject.avatar;

  userObject._id.toString();

  return userObject;
};

module.exports.User = mongoose.model('User', userSchema);
