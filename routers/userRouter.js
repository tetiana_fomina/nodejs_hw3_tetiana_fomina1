const express = require('express');
const router = new express.Router();
const auth = require('../middlewares/auth');
const bcrypt = require('bcrypt');
const multer = require('multer');
const sharp = require('sharp');
const PDFDocument = require('pdfkit');

router.get('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    res.status(200).json({user: user});
  } else {
    res.status(400).json({message: 'Please autheticate!'});
  }
});

router.delete('/', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (user.role === 'SHIPPER') {
      try {
        user = [];
        await user.save();
        res.status(200).json({message: 'Success'});
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    } else {
      res.status(400).json({message: 'You can"t delete your account!'});
    }
  } else {
    res.status(400).json({message: 'Please autheticate!'});
  }
});

router.patch('/password', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    const {oldPassword, newPassword} = req.body;
    if (oldPassword && newPassword) {
      if (!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({message: `Wrong password!`});
      } else {
        if (newPassword != oldPassword) {
          user.password = await bcrypt.hash(newPassword, 10);
          await user.save();
          res.status(200).json({message: 'Success'});
        }
      }
    } else {
      res.status(400).json({message: 'Enter all needed credentials!'});
    }
  } else {
    res.status(400).json({message: 'Please autheticate!'});
  }
});

const upload = multer({
  limits: {
    fileSize: 1000000,
  },
  fileFilter(req, file, callback) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return callback(new Error('Please upload only pictures!'));
    }
    callback(undefined, true);
  },
});

router.post(
    '/avatar',
    auth,
    upload.single('avatar'),
    async (req, res) => {
      const buffer = await sharp(req.file.buffer)
          .resize({width: 250, height: 250})
          .png()
          .toBuffer();
      req.user.avatar = buffer;
      await req.user.save();
      res.status(200).json({message: 'Picture is uploaded!'});
    },
    (error, req, res, next) => {
      if (!user) {
        return res.status(400).json({message: 'Please authenticate!'});
      }
      res.status(400).send({error: error.message});
    },
);

router.get('/avatar', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    if (!user.avatar) {
      return res.status(400).json({
        message: 'You have not avatar',
      });
    }
    res.set('Content-Type', 'image/png');
    res.status(200).send(user.avatar);
  } else {
    res.status(400).json({message: 'Please autheticate!'});
  }
});

router.get('/reports', auth, async (req, res) => {
  const user = req.user;
  if (user) {
    const doc = new PDFDocument();
    doc.pipe(res);
    if (user.role === 'SHIPPER') {
      try {
        let loads = await user
            .populate({
              path: 'loads',
            })
            .execPopulate();
        loads = loads.filter((load) => load.status === 'SHIPPED');
        if (loads.length > 0) {
          doc.text(JSON.parse(loads), 100, 100);
          doc.end();
          res.download(doc);
        } else {
          res.status(400).json({message: 'You haven"t shipped loads'});
        }
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    } else {
      res.status(400).json({message: 'You are not shipper!'});
    }
  } else {
    res.status(400).json({message: 'Please autheticate!'});
  }
});

module.exports = router;
