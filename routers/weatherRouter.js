const axios = require('axios').default;
const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {WEATHER_API_KEY} = require('../config');

router.get(
    '/:city',
    asyncWrapper(async (req, res) => {
      try {
        const response = await axios.get(
            `http://api.openweathermap.org/data/2.5/weather?q=${req.params.city}&appid=${WEATHER_API_KEY}&units=metric`,
        );
        res.json({
          weather: response.weather,
          main: response.main,
          city: response.name,
        });
      } catch (e) {
        res.status(400).json({message: e.message});
      }
    }),
);

module.exports = router;
